# RNN
import numpy as np
import csv
import operator

import sys
import json
import math


def read_data(filename):  # read csv file and create an 2D array
    _datafile = open('./ChatBot_json/'+filename, 'r', errors='ignore')
    csv_data = csv.reader(_datafile, delimiter=',')
    dataset = []
    for raw in csv_data:
        raw_data = []
        for each_data in raw:
            raw_data.append(int(each_data))
        dataset.append(raw_data)
    dataset = np.array(dataset)
    return dataset


def get_data_file(filename):
    data_file = open(filename, 'r')
    if data_file.read() == '':
        data = ''
    else:
        with open(filename) as f:
            data = json.load(f)
    return data


def set_data_file(filename, data):
    # save parameters in JSON file
    with open(filename, 'w') as file:
        json.dump(data, file)


def truncate(f, n):
    return math.floor(f * 10 ** n) / 10 ** n


def softmax(x):
    xt = np.exp(x - np.max(x))
    return xt / np.sum(xt)


class RNN:
    def __init__(self, input_dim, hidden_size=100, bptt_truncate=4):

        self.number_instances = 0  # total number of instances
        self.train_length = 0  # size of the training sample
        self.test_length = 0  # size of the test sample

        self.input_dim = input_dim
        self.hidden_size = hidden_size
        self.BPTT_truncate = bptt_truncate

        # Temp_parameters
        self.U = np.random.rand(hidden_size, input_dim)
        self.W = np.random.rand(hidden_size, hidden_size)
        self.V = np.random.rand(input_dim, hidden_size)
        """
        weights = get_data_file('weights.json')
        if weights is '':
            
            weights_data = {'U': self.U, 'V': self.V, 'W': self.W}
            set_data_file('weights.json', weights_data)
        else:
            self.U = weights['U']
            self.W = weights['W']
            self.V = weights['V']
        """
        self.St_1 = np.zeros((hidden_size, input_dim))

    def feedforward(self, x):
        if isinstance(x[0], float):
            T = 1
            x = np.array(x)
            x = x.reshape(1, x.shape[0])
        else:
            T = len(x)

        s = np.zeros((T, self.hidden_size))
        s[-1] = np.zeros(self.hidden_size)
        o = np.zeros(x.shape)

        for t in range(T):
            s[t] = np.tanh(np.dot(self.U, x[t]) + np.dot(self.W, s[t - 1]))
            o[t] = softmax(np.dot(self.V, s[t]))
        return [o, s]

    def predict(self, x):
        o, s = self.feedforward(x)

        #weights = {"U": self.U.tolist(), "V": self.V.tolist(), "W": self.W.tolist()}
        #set_data_file('../ChatBot_json/weights.json', weights)

        return o[-1]  # np.argmax(o, axis=1)

    def calculate_loss(self, x, y):
        N = len(y)
        loss = 0
        total_loss = 0
        for i in range(N):
            actual_y = y[i]
            o, s = self.feedforward(x[i])
            predict_y = o[0]

            for index in range(len(actual_y)):
                loss += ((actual_y[index] - predict_y[index]) ** 2)
            total_loss += loss
        return total_loss / N

    def bptt(self, x, y):
        if isinstance(x[0], float):
            T = 1
            x = np.array(x)
            x = x.reshape(1, x.shape[0])
        else:
            T = len(x)

        o, s = self.feedforward(x)

        dLdU = np.zeros(self.U.shape)
        dLdW = np.zeros(self.W.shape)
        dLdV = np.zeros(self.V.shape)

        delta_o = o - y

        for t in np.arange(T):
            # at time step t, shape is input_dim * hidden_size
            # dLdV += np.outer(delta_o[t], s[t].T)
            delta_o = np.array(delta_o)

            dLdV += np.outer(delta_o[t], s[t])

            delta_t = self.V.T.dot(delta_o[t]) * (1.0 - (s[t] ** 2))  # ^2 => **2

            for bptt_step in np.arange(max(0, t - self.BPTT_truncate), t + 1)[::-1]:
                sb = np.reshape(s[bptt_step - 1], (1, s[bptt_step - 1].shape[0])).T  # shape (1,10)

                # dLdW += np.outer(delta_t, s[bptt_step - 1])
                dLdW += np.dot(delta_t, sb)

                # dLdU[:, x[bptt_step]] += delta_t
                delta_t = np.array(delta_t)
                dLdU += np.outer(delta_t, x)

                delta_t = self.W.T.dot(delta_t) * (1 - s[bptt_step - 1] ** 2)  # ^2 => **2

        return [dLdU, dLdV, dLdW]

    def gradient_check(self, x, y, h=0.001, error_threshold=0.01):
        bptt_gradients = self.bptt(x, y)
        model_parameters = ["U", "V", "W"]

        for pidx, pname in enumerate(model_parameters):
            parameters = operator.attrgetter(pname)(self)

            # print("performing gradient check for parameter %s with size %d. " % (pname, np.prod(parameters.shape)))

            it = np.nditer(parameters, flags=['multi_index'], op_flags=["readwrite"])
            while not it.finished:
                ix = it.multi_index
                original_values = parameters[ix]  # save original values

                # estimate the gradient using (f(x+h) - f(x-h))/2h

                parameters[ix] = original_values + h
                gradplus = self.calculate_loss([x], [y])
                parameters[ix] = original_values - h
                gradminus = self.calculate_loss([x], [y])

                estimated_gradients = (gradplus - gradminus) / (2 * h)

                # reset original value
                parameters[ix] = original_values

                backpropagation_gradients = bptt_gradients[pidx][ix]

                # calculate the relative error (|x - y|)/(|x|+|y|)

                relative_error = np.abs(backpropagation_gradients - estimated_gradients) / (
                        np.abs(backpropagation_gradients) + np.abs(estimated_gradients))

                if relative_error < error_threshold:
                    # print("Gradient check error: parameter = %s ix = %s" % (pname, ix))
                    # print("+h Loss: %f" % gradplus)
                    # print("-h Loss: %f" % gradminus)
                    # print("Estimated gradient: %f" % estimated_gradients)
                    # print("Backpropagation gradient: %f" % backpropagation_gradients)
                    # print("Relative error: %f" % relative_error)
                    return
                it.iternext()
            # print("Gradient check for parameter %s passed. " % pname)

    def sgd(self, x, y, learning_rate):
        dLdU, dLdV, dLdW = self.bptt(x, y)
        self.U -= learning_rate * dLdU
        self.V -= learning_rate * dLdV
        self.W -= learning_rate * dLdW

    def sgd_training_model(self, model, training_data, testing_data, learning_rate=0.005, nepoch=10,
                           evaluate_loss_after=5):

        self.number_instances = len(training_data) + 1
        self.train_length = len(training_data)
        self.test_length = len(testing_data)

        x = training_data
        y = testing_data

        losses = []
        num_examples_seen = 0

        for epoch in range(nepoch):
            if epoch % evaluate_loss_after == 0:
                loss = model.calculate_loss(x, y)

                losses.append((num_examples_seen, loss))

                # time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                # print("%s: loss after num_examples_seen=%d epoch=%d: %f" % (time, num_examples_seen, epoch, loss))

                if len(losses) > 1 and losses[-1][1] > losses[-2][1]:
                    learning_rate = learning_rate * 0.5
                    # print("setting learning rate to %f" % (learning_rate))
                sys.stdout.flush()
            for i in range(len(y)):
                model.sgd(x[i], y[i], learning_rate)
                num_examples_seen += 1

        """U = self.U.tolist()
        V = self.V.tolist()
        W = self.W.tolist()
        
        with open('./weights.json', 'w') as weigths:
            json.dump({'U': U, 'V': V, 'W': W}, weigths)
        """

