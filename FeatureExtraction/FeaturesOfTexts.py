import nltk

from FeatureExtraction import TextPreProcessing

class FeaturesOfTexts:
    def __init__(self, raw_data):
        self.feature_list = []
        self.raw_data = nltk.word_tokenize(raw_data)

    def set_feature_list(self, data_tokens):
        # text pre-processing
        text_pre_process = TextPreProcessing.TextPreProcess(data_tokens)
        token_list = text_pre_process.get__text_tokens()

        for token in token_list:
            if token not in self.feature_list:
                self.feature_list.append(token)

    def get_feature_list(self):
        self.set_feature_list(self.raw_data)
        return self.feature_list
