import nltk

class sqlStatment:
    def __init__(self, user_response):
        self.user_response = user_response
        self.attributes = []
        self.values= []
        self.condition = []

    def getTags(self, user_response):
        words = nltk.word_tokenize(user_response)
        tags = nltk.pos_tag(words)
        return tags

    def setSQLParameters(self):
        tags = sqlStatment.getTags(self, self.user_response)
        getQueryData = r"""
                Attribute : {<NN.*>}
                AttributeTarget: {<Attribute><CC><Attribute>+}
                Prefix: {<DT|JJ>}
                AttributeSection: {<Prefix><AttributeTarget><Postfix>}
                Condition: {<V.*>|<JJR><IN>|<JJS><IN>|<JJR>|<JJS>|<RBR>|<RBR><IN>|<NNS>}
                values: {<CD>}
                """
        resultParser = nltk.RegexpParser(getQueryData)
        result = resultParser.parse(tags)

        for subtree in result.subtrees():
            if subtree.label() == "AttributeTarget":
                for each_subtree in subtree:
                    if type(each_subtree) is not tuple and each_subtree.label() == "Attribute":
                        #self.attributes.append(each_subtree[0][0]) #removed temporary
                        self.values.append(each_subtree[0][0])
            elif subtree.label() == "Attribute" and subtree[0][0] not in self.attributes:
                self.values.append(subtree[0][0])
            elif subtree.label() == "values" and subtree[0][0] not in self.attributes:
                self.values.append(subtree[0][0])

        for subtree in result.subtrees():
            if subtree.label() == "Condition":
                for each_data in subtree:
                    #self.condition = each_data[0]
                    self.condition.append(each_data[0])

        sqlParameters = {"query": self.user_response,"attributes": self.attributes, "values":self.values, "condition": self.condition}
        return sqlParameters
