# ChatBot API for GET & POST data with the database
import urllib3
import json


def getdata(key,session_id):
    http = urllib3.PoolManager()
    r = http.request('GET', 'http://localhost:3000/'+key+'/'+session_id)
    return json.loads(r.data.decode('utf-8'))

def postdata(key,session_id,data):
    http = urllib3.PoolManager()
    encoded_data = json.dumps(data).encode('utf-8')
    http.request('PUT', 'http://localhost:3000/'+key+'/'+session_id, body=encoded_data,
                 headers={'Content-Type': 'application/json'})

