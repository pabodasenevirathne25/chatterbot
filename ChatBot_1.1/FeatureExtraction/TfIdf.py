from FeatureExtraction import TextPreProcessing

import nltk
import math
import json


def set_data_file(filename, data):
    # save parameters in JSON file
    with open(filename, 'w') as file:
        json.dump(data, file)


def get_data_file(filename):
    data_file = open(filename, 'r')
    if data_file.read() == '':
        data = ''
    else:
        with open(filename) as f:
            data = json.load(f)
    return data


def text_pre_process(sentence):
    sentence_tokens = nltk.word_tokenize(sentence)
    try:
        words_list = TextPreProcessing.TextPreProcess(sentence_tokens).get__text_tokens()
    except IndexError:
        words_list = []
        print('Error: Empty word_list in ChatbotSystem/TfIdf.py')
    return words_list


def get_tf_value(word, sentence):
    words_list = text_pre_process(sentence)
    count = words_list.count(word)
    return count / len(words_list)


def get_idf_value(word, sentences):
    count = 0
    for sentence in sentences:
        words_list = text_pre_process(sentence)
        if word in words_list:
            count += 1
    idf_value = 1 + math.log(len(sentences) / 1 + count)
    return idf_value


class TfIdf:
    def __init__(self):
        self.question_2d_tiarray = []
        self.user_response_tiarray = []
        self.sentences = ''
        self.features_list = []

    def set_raw_feature_data(self, raw_data, features_list):  # raw data --> multiple sentences
        self.sentences = nltk.sent_tokenize(raw_data)
        self.features_list = features_list

    def get_tf_idf_sentence_array(self, sentence):
        sentence_tf_idf = []
        for word_feature in self.features_list:
            tf = get_tf_value(word_feature, sentence)
            idf = get_idf_value(word_feature, self.sentences)
            tf_idf_value = tf * idf
            sentence_tf_idf.append(tf_idf_value)
        return sentence_tf_idf

    def get_user_response_tfidf(self, user_response):
        self.user_response_tiarray = self.get_tf_idf_sentence_array(user_response)
        return self.user_response_tiarray

    def get_sentences_tfidf(self):
        self.question_2d_tiarray = []
        for sentence in self.sentences:
            self.question_2d_tiarray.append(self.get_tf_idf_sentence_array(sentence))
        return self.question_2d_tiarray

