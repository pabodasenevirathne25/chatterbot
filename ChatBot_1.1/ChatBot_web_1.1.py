import cgi
import cgitb
import json
import ChatBot
import nltk
import random

cgitb.enable() #for debugging

print("Content-type: text/html")
print()
print("<html><head>")
print("")

print("Hello. My name is DoxPro-Chatbot. I will answer your queries about DoxPro. If you want to exit, type Bye!<br><br>")
print("<form id='message-form' name='chatbot' action='/cgi-bin/ChatBot/ChatBot_1.1/ChatBot_web_1.1.py' method='get'>")
print("Me: <input id='messege-input' type='text' size = 100 name='myquery'>")
print("<input type='submit' value='send'>")
print("</form>")


form = cgi.FieldStorage()
#user_responce = ""
user_responce =  form.getvalue('myquery')

def get_data_file(filename):
    data_file = open(filename, 'r')
    if data_file.read() == '':
        data = ''
    else:
        with open(filename) as f:
            data = json.load(f)
    return data

def set_data_file(filename, data):
    # save parameters in JSON file
    with open(filename, 'w') as file:
        json.dump(data, file)
def set_okay():
    Okay = ["absolutely", "agreed", "all right", "aye", "by all means", "certainly", "fine", "good", "good enough",
            "just so", "of course", "okay", "positively", "precisely", "sure thing", "surely", "very well", "yea",
            "yep"]
    return random.choice(Okay)

class ChatBotWeb:

    def __init__(self):

        c = ChatBot.ChatBot()

        self.bot_responce = ""

        flag = True

        to_ask = False
        to_ask_parameters = get_data_file('to_ask.json')
        if to_ask_parameters is '':
            to_ask_parameters = {'to_ask': to_ask}
            set_data_file('to_ask.json', to_ask_parameters)
        else:
            to_ask = to_ask_parameters['to_ask']

        self.user_responce = user_responce
        if self.user_responce is not None:
            user_responces = self.user_responce.split('and')

            self.t = 0  # Time
            self.index_A = []
            self.index_Ya = []
            parameters = get_data_file('Temp_parameters.json')
            if parameters is '':
                parameters = {'t': self.t, 'index_A': self.index_A, 'index_Ya': self.index_Ya}
                set_data_file('Temp_parameters.json', parameters)
            else:
                self.t = parameters['t']
                self.index_A = parameters['index_A']
                self.index_Ya = parameters['index_Ya']

            self.t += 1
            for one_responce in user_responces:

                if to_ask is True:
                    if one_responce == 'yes':
                        answers = c.answers
                        answers_array = nltk.sent_tokenize(answers)
                        idy = self.index_Ya[-1]
                        answer = answers_array[idy]
                        bot_response = (answer, flag)
                        self.bot_responce = bot_response[0]
                        self.toprint()
                        to_ask = False
                        to_ask_parameters = {'to_ask': to_ask}
                        set_data_file('to_ask.json', to_ask_parameters)

                    elif one_responce == 'no':
                        answer = set_okay() + '... Do you have any other questions?'
                        bot_response = (answer, flag)
                        self.bot_responce = bot_response[0]
                        self.toprint()
                        to_ask = False
                        to_ask_parameters = {'to_ask': to_ask}
                        set_data_file('to_ask.json', to_ask_parameters)
                    else:
                        bot_response = c.set_response(one_responce, flag, self.t)
                        self.bot_responce = bot_response[0]
                        self.toprint()

                        if not bot_response[1]:
                            self.t = 0  # Time
                            self.index_A = []
                            self.index_Ya = []
                            parameters = {'t': self.t, 'index_A': self.index_A, 'index_Ya': self.index_Ya}
                            set_data_file('Temp_parameters.json', parameters)

                            self.user_response_array = []
                            data = {'Training_data': self.user_response_array}
                            set_data_file('Training_data.json', data)

                        to_ask = False
                        to_ask_parameter = {'to_ask': to_ask}
                        set_data_file('to_ask.json', to_ask_parameter)

                else:
                    bot_response = c.set_response(one_responce, flag, self.t)
                    self.bot_responce = bot_response[0]
                    self.toprint()

                    if not bot_response[1]:
                        self.t = 0  # Time
                        self.index_A = []
                        self.index_Ya = []
                        parameters = {'t': self.t, 'index_A': self.index_A, 'index_Ya': self.index_Ya}
                        set_data_file('Temp_parameters.json', parameters)

                        self.user_response_array = []
                        data = {'Training_data': self.user_response_array}
                        set_data_file('Training_data.json', data)

                        to_ask = False
                        to_ask_parameter = {'to_ask': to_ask}
                        set_data_file('to_ask.json', to_ask_parameter)

                    if self.t % 3 == 0 and self.t != 0:
                        print("<br>")
                        c.get_bot_question()
                        to_ask = True
                        to_ask_parameters = {'to_ask': to_ask}
                        set_data_file('to_ask.json', to_ask_parameters)

    def toprint(self):
        print(self.bot_responce)


if __name__ == '__main__':
    c = ChatBotWeb()

print ("</body></html>")