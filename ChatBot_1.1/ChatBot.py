from NeuralNetworks import Rnn
from SimilarityMeasures import CosineSimilarity
from FeatureExtraction import FeaturesOfTexts, TfIdf

# import HandleUserResponse_1
import random
import json
import numpy as np
import nltk
import re


def set_greeting(user_response):
    greeting_inputs = ("hello", "hi", "greetings", "sup", "what's up", "hey")
    greeting_responses = ["hi", "hey", "hi there", "hello", "I am glad! you are talking to me"]
    for word in user_response.split():
        if word.lower() in greeting_inputs:
            return random.choice(greeting_responses)


def set_okay():
    Okay = ["absolutely", "agreed", "all right", "aye", "by all means", "certainly", "fine", "good", "good enough",
            "just so", "of course", "okay", "positively", "precisely", "sure thing", "surely", "very well", "yea",
            "yep"]
    return random.choice(Okay)


def set_data_file(filename, data):
    # save parameters in JSON file
    with open(filename, 'w') as file:
        json.dump(data, file)


def get_data_file(filename):
    data_file = open(filename, 'r')
    if data_file.read() == '':
        data = ''
    else:
        with open(filename) as f:
            data = json.load(f)
    return data


def set_corpus(filename):
    file = open(filename, 'r', errors='ignore')
    raw = file.read()
    raw = raw.lower()
    return raw


class ChatBot:
    def __init__(self):
        # set corpus

        answers = set_corpus('Answer_Corpus.txt')
        defined_questions = set_corpus('Question_Corpus.txt')
        defined_bot_questions = set_corpus('RobotQuestion_Corpus.txt')

        self.main_corpus_data = set_corpus('Corpus.txt')

        # set answers tokens
        self.answers = answers

        # set defined questions tokens
        self.questions = defined_questions

        # set defined bot questions tokens
        self.defined_bot_questions = defined_bot_questions

        self.user_response = ''
        self.bot_response = ''

        # Temp_parameters
        self.t = 0  # Time
        self.index_A = []
        self.index_Ya = []
        parameters = get_data_file('Temp_parameters.json')
        if parameters is '':
            parameters = {'t': self.t, 'index_A': self.index_A, 'index_Ya': self.index_Ya}
            set_data_file('Temp_parameters.json', parameters)
        else:
            self.t = parameters['t']
            self.index_A = parameters['index_A']
            self.index_Ya = parameters['index_Ya']

        # Training data
        self.user_response_array = []
        user_response = get_data_file('Training_data.json')
        if user_response is '':
            data = {'Training_data': self.user_response_array}
            set_data_file('Training_data.json', data)
        else:
            self.user_response_array = user_response['Training_data']

        self.feature_list = []
        feature_list = get_data_file('FeatureLists.json')
        if feature_list is '':
            feature_object = FeaturesOfTexts.FeaturesOfTexts(self.main_corpus_data)
            self.feature_list = feature_object.get_feature_list()

            data = {'feature_list': self.feature_list}
            set_data_file('FeatureLists.json', data)
        else:
            self.feature_list = feature_list['feature_list']

        # Get tf-idf values of Questions
        self.question_tfidf_data = []
        tfidf_data = get_data_file('TfIdfData.json')
        if tfidf_data is '':
            q = TfIdf.TfIdf()
            q.set_raw_feature_data(self.questions, self.feature_list)
            question_tfidf_data = q.get_sentences_tfidf()
            self.question_tfidf_data = question_tfidf_data
            data = {'question_tfidf_data': self.question_tfidf_data}
            set_data_file('TfIdfData.json', data)
        else:
            self.question_tfidf_data = tfidf_data['question_tfidf_data']

        self.question_similarity = CosineSimilarity.CosineSimilarity()

        self.idy = -1

    def get_bot_question(self):
        user_responses = '. \n'.join(self.user_response_array)
        feature_list = self.feature_list
        defined_question_tfidf_data = self.question_tfidf_data

        t2 = TfIdf.TfIdf()
        t2.set_raw_feature_data(user_responses, feature_list)
        user_inputs_tfidf_data = t2.get_sentences_tfidf()

        training_data = np.array(user_inputs_tfidf_data)[:-1]
        testing_data = np.array(user_inputs_tfidf_data)[1:]

        # use RNN from Neural Network package
        feature_size = np.array(user_inputs_tfidf_data).shape[1]

        # create a model in RNN
        model = Rnn.RNN(feature_size)

        # train the model to update parameters using back-propagation and calculate loss
        model.sgd_training_model(model, training_data, testing_data)

        prediction_array = model.predict(training_data)
        bot_q_similarity = CosineSimilarity.CosineSimilarity()

        # get bot question index
        prediction = prediction_array
        sentences_data = np.array(defined_question_tfidf_data)

        cosine_array = bot_q_similarity.get_cosine_array_for_bot(prediction, sentences_data)

        bot_question_array = re.split(r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.)\s', self.defined_bot_questions)

        parameters = get_data_file('Temp_parameters.json')
        index_A = parameters['index_A']
        self.index_Ya = parameters['index_Ya']
        self.idy = -1
        max_value = 0
        bot_response = ''
        for i in range(len(cosine_array)):
            bot_question = bot_question_array[i]
            if (i not in index_A) and (i not in self.index_Ya) and (bot_question != '.'):
                if max_value < cosine_array[i]:
                    max_value = cosine_array[i]
                    self.idy = i
                    bot_response = bot_question_array[self.idy]

        self.index_Ya.append(self.idy)
        parameters = {'t': self.t, 'index_A': index_A, 'index_Ya': self.index_Ya}
        set_data_file('Temp_parameters.json', parameters)
        print(bot_response)

    def set_response(self, user_response, flag, t):
        user_response = user_response.lower()
        self.t = t
        if flag is True:
            if user_response == 'bye':
                flag = False
                self.bot_response = 'Bye! take care..'
            elif user_response == 'thanks' or user_response == 'thank you':
                flag = False
                self.bot_response = 'you are welcome..'
            elif set_greeting(user_response) is not None:
                self.bot_response = set_greeting(user_response)
            else:
                # get cosine similarity values
                feature_list = self.feature_list
                question_tfidf_data = self.question_tfidf_data

                u = TfIdf.TfIdf()
                u.set_raw_feature_data(self.questions, feature_list)
                user_tfidf_data = u.get_user_response_tfidf(user_response)

                cosine_values = self.question_similarity.get_cosine_array(user_tfidf_data, question_tfidf_data)
                max_cosine_value = np.max(cosine_values[:-1])
                answer_index = 0
                for i in range(len(cosine_values) - 1):
                    if max_cosine_value == cosine_values[i]:
                        self.index_A.append(i)
                        answer_index = i
                        parameter_data = {'t': self.t, 'index_A': self.index_A, 'index_Ya': self.index_Ya}
                        set_data_file('Temp_parameters.json', parameter_data)

                flat = cosine_values.flatten()
                flat.sort()
                req_tfidf = flat[-2]
                if req_tfidf == 0:
                    #handle_response = HandleUserResponse_1.HandleResponse()
                    #print(handle_response.get_bot_response(user_response))
                    print("I haven't got your message! You can ask me about this web site..")
                else:
                    self.bot_response = nltk.sent_tokenize(self.answers)[answer_index]

                # Collect user's responses to input for RNN
                self.user_response_array.append(user_response)
                data = {'Training_data': self.user_response_array}
                set_data_file('Training_data.json', data)
            return self.bot_response, flag

    def toPrint(self):
        print(self.bot_response)

    def setResponce(self):
        user_input = input('\nMe: ')
        self.user_response = user_input

    def setResponceByID(self, user_input):
        self.user_response = user_input

    def main(self):
        # get user response
        flag = True
        while flag:
            #self.user_response = input('\nMe: ')
            self.setResponce()
            user_responses = self.user_response.split('and')
            self.t += 1

            for one_response in user_responses:
                bot_response = self.set_response(one_response, flag, self.t)
                print(bot_response[0])
                if not bot_response[1]:
                    self.t = 0  # Time
                    self.index_A = []
                    self.index_Ya = []
                    parameters = {'t': self.t, 'index_A': self.index_A, 'index_Ya': self.index_Ya}
                    set_data_file('Temp_parameters.json', parameters)

                    self.user_response_array = []
                    data = {'Training_data': self.user_response_array}
                    set_data_file('Training_data.json', data)

                    return

                if self.t % 3 == 0 and self.t != 0:
                    # get bot question
                    self.get_bot_question()
                    #user_responses = input('\nMe: ')
                    self.setResponce()
                    user_responses = self.user_response.lower()

                    if user_responses == 'yes':
                        answers = self.answers
                        answers_array = nltk.sent_tokenize(answers)
                        answer = answers_array[self.idy]
                        bot_response = (answer, flag)
                        print(bot_response[0])
                    elif user_responses == 'no':
                        answer = set_okay() + '... Do you have any other questions?'
                        bot_response = (answer, flag)
                        print(bot_response[0])

                    else:
                        user_responses = user_responses.split('and')
                        for response in user_responses:
                            bot_response = self.set_response(response, flag, self.t)
                            print(bot_response[0])
                            if not bot_response[1]:
                                self.t = 0  # Time
                                self.index_A = []
                                self.index_Ya = []
                                parameters = {'t': self.t, 'index_A': self.index_A, 'index_Ya': self.index_Ya}
                                set_data_file('Temp_parameters.json', parameters)

                                self.user_response_array = []
                                data = {'Training_data': self.user_response_array}
                                set_data_file('Training_data.json', data)

                                return


if __name__ == "__main__":
    c = ChatBot()
    c.main()
