import math
import numpy as np


def get_cosine_value(v1, v2):

    dot_sum = 0.0
    sum_norm1 = 0.0
    sum_norm2 = 0.0
    v1 = np.array(v1)
    v2 = np.array(v2)
    for i in range(len(v1)):
        x = v1[i]
        y = v2[i]
        # Dot product
        dot_sum += x * y
        # Norm
        sum_norm1 += x * x
        sum_norm2 += y * y

    if math.sqrt(sum_norm1 * sum_norm2) > 0:
        return dot_sum / math.sqrt(sum_norm1 * sum_norm2)
    else:
        return 0


class CosineSimilarity:
    """
    @staticmethod
    def get_cosine_array(sentence, raw_data):
        tf_idf = TfIdf.TfIdf(raw_data)
        cosine_array = []
        user_input_array = tf_idf.get_tf_idf_sentence_array(sentence)
        sentences_array = tf_idf.get_tf_idf_2d_array()

        for each_sentence_vector in sentences_array:
            cosine_array.append(get_cosine_value(user_input_array, each_sentence_vector))
        # return as a numpy array
        return np.array(cosine_array)
    """

    @staticmethod
    def get_cosine_array(user_response_vector, question_vectors):
        cosine_array = []
        for each_vector in question_vectors:
            cosine_array.append(get_cosine_value(user_response_vector, each_vector))
        # return as a numpy array
        return np.array(cosine_array)

    @staticmethod
    def get_cosine_array_for_bot(predicted_array, d2_array):
        cosine_array = []
        for each_vector in d2_array:
            cosine_array.append(get_cosine_value(predicted_array, each_vector))
        return np.array(cosine_array)